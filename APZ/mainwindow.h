#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QLocale>
#include <QStringList>
#include <QList>
#include <QRegExp>
#include <qmath.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_SLOC_clicked();

    void on_Empty_clicked();

    void on_Komment_clicked();

    void on_Only_clicked();

    void on_VocabOpand_clicked();

    void on_VocabOper_clicked();

    void on_KvoOper_clicked();

    void on_KvoOpand_clicked();

    void on_Cyclomatic_clicked();

    void on_Vocablurary_clicked();

    void on_N_clicked();

    void on_V_clicked();

    void on_Diff_clicked();

    void on_Exit_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
