#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_KSVM_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
      float s1 = lst.count()-1;
      int s=0;
    for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            s=s+item.count("\n");
        }
        float s2=(float)s/(lst.count())-1;

       ui->label->setText(QString::number(s2));
}

void MainWindow::on_KvoMet_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
      float s1 = lst.count()-1;
      int s=0;
    for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            s=s+item.count("\n");
        }
        float s2=(float)s/(lst.count())-1;

       ui->label_2->setText(QString::number(s1));
}

void MainWindow::on_KvoKlass_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    lst = code.split(QRegExp("class "), QString::SkipEmptyParts);
    float s3 = lst.count()-1;
    ui->label_4->setText(QString::number(s3));
}

void MainWindow::on_KSVK_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    int s=0;
    lst = code.split(QRegExp("class "), QString::SkipEmptyParts);
    float s3 = lst.count()-1;
    s=0;
        for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            s=s+item.count("\n")-1;
        }
        float s4=(float)s/(lst.count());
        ui->label_3->setText(QString::number(s4));
}

void MainWindow::on_KvoModul_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    lst = code.split(QRegExp("#include"), QString::SkipEmptyParts);
        float s5=lst.count();
        ui->label_5->setText(QString::number(s5));

}

void MainWindow::on_KUModul_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    lst = code.split(QRegExp("class "), QString::SkipEmptyParts);
        int s=0;
        for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            QStringList list=item.split("){",QString::SkipEmptyParts);
            s=s+item.count(")")-item.count("this->")-item.count(")");
        }
        ui->label_6->setText(QString::number(s));
}
void MainWindow::on_WMC_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    int s=0;
    lst = code.split(QRegExp("class "), QString::SkipEmptyParts);
    float s3 = lst.count()-1;
    s=0;
        for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            s=s+item.count("\n")-1;
        }
        float s4=(float)s/(lst.count());
        s=0;
        for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            QStringList list=item.split("){",QString::SkipEmptyParts);
            for(int j=list.count()-1;j>0;--j)
            {
                const QString& ite=list[j];
                if ((ite.count("{")>=ite.count("}"))&&(ite.count("}")!=0))
                 s=s+1;
            }
        }
        ui->label_7->setText(QString::number(s));
}

void MainWindow::on_RFC_clicked()
{
    QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    int s=0;
        for(int i=lst.count()-1;i>=0;i--)
        {
            const QString& item=lst[i];
            QStringList list=item.split("){",QString::SkipEmptyParts);
            s=s+item.count(")")-item.count(")");
        }
        ui->label_8->setText(QString::number(s));
}

void MainWindow::on_NOC_clicked()
{    ui->label_9->setText("");
         QString code= ui->plainTextEdit->toPlainText();
        QStringList lst = code.split(QRegExp("class"),  QString::SkipEmptyParts);
        QStringList lis, Lclss, Rclss;
        for (int g = 0; g<lst.count(); g++)
        {        const QString& item = lst[g].trimmed();
            QString St ="";
            for(int i=0; i<item.count(); i++)
                if (item[i]!='\n')
                    St=St+QString(item[i]);
                else
                    break;
            lis<<St;
        }
        for (int g = 0; g<lis.count(); g++)
        {        const QString& item = lis[g].trimmed();
            QString St ="";
            for(int i=0; i<item.count(); i++)
                if (item[i]!=' ')
                    St=St+QString(item[i]);
                else break;
            Rclss<<St;
        }
        for (int g = 0; g<lis.count(); g++)
        {        const QString& item = lis[g].trimmed();
            QString St ="";
            lst = item.split(QRegExp("public|protected|private "),  QString::SkipEmptyParts);
            for(int i=0; i<lst.count(); i++)
            {            St = St+lst[i];        }
            Lclss<<St;
        }
        for (int g = 0; g<Rclss.count(); g++)
        {        Lclss[g].replace(0,Rclss[g].count(),"");
            Lclss[g] = Lclss[g].trimmed();    }
        for (int g = 0; g<Rclss.count(); g++)
        {        Rclss[g].remove(":");
            int s = 0;
            for (int i = 0; i<Rclss.count(); i++)
            {s=s+Lclss[i].count(Rclss[g]);}
                    ui->label_9->setText( ui->label_9->text()+" "+Rclss[g]+" "+QString::number(s));
            //ui->label_9->setText(QString::number(s));
        }

  /*  QString code= ui->plainTextEdit->toPlainText();
    QStringList lst = code.split(QRegExp("(void|int|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
    //QStringList lst = code.split(QRegExp("class\s"), QString::SkipEmptyParts);
        QStringList lis, Lelss, Relss;
        for(int g=0;g<lst.count();g++)
        {
            const QString& item=lst[g].trimmed();
            QString St="";
            for(int i=0;i<item.count();i++)
            {
                if (item[i]!='\n')
                    St=St+QString(item[i]);
                else
                    break;
            }
            lis<<St;
        }
        for(int g=0;g<lst.count();g++)
        {
            const QString& item=lst[g].trimmed();
            QString St="";
            for(int i=0;i<item.count();i++)
            {
                if (item[i]!=' ')
                    St=St+QString(item[i]);
                else
                    break;
            }
            Relss<<St;
        }
        for(int g=0;g<lis.count();g++)
        {
            const QString& item=lst[g].trimmed();
            QString St="";
            lst = code.split(QRegExp("public|protected|private"), QString::SkipEmptyParts);
            for(int i=0;i<item.count();i++)
            {
                St=St+lst[i];
            }
            Lelss<<St;
        }
        for(int g=0;g<Relss.count();g++)
        {
            Lelss[g].replace(0,Relss.count(),"");
            Lelss[g]=Lelss[g].trimmed();
        }
        int s=0;
        for(int g=0;g<Relss.count();g++)
        {
            Lelss[g].remove(":");
            for(int i=0;i<Relss.count();g++)
            {
                s=s+Lelss[i].count(Relss[g]);
            }
        }
        ui->label_9->setText(QString::number(s));*/
}

void MainWindow::on_Exit_clicked()
{
    close();
}
